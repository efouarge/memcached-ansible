Memcached Configuration and Automation
===================

Overview
----------
This runbook will install and config memcached and the required dependencies with a few operational tasks to go along with it.


System Requirements
-------------

Outlined are the steps required to start working with this solutiuon.
> **Base Requirements to get started:**

> - CentOS 6 x64 
> - git
> - ansible


#### Basic Requirements

You will need to make sure that you have ansible configured and have a targeted host group for this solution to work properly.  You will need sudo access, and you also must be aware some of these scripts and workflows are not destructive by nature.   Currently there is no feedback loops nor outside testing and assertions created here.

#### Step 1 Installing Memcached

Outlined are the steps required to get the base infrastructure setup so the scripts will run properly.
> **Important:**
This will only install memcached, it will not configure PHP to use memcached.  You will need to do that outside this runbook.



    ansible-playbook memcache-install.yml --extra-vars "hosts={TargetGroup}"


#### Step 2 Flushing Memcached

Outlined are the steps required to flush memcached gracefully
> **Important:**
This will drop ALL memcache keys that are currently stored in memcached and you will likely take a performance hit on the applications that are using this to cache data.



    ansible-playbook memcache-flush.yml --extra-vars "hosts={TargetGroup}"

